/**
 * Created by stone on 8.5.17..
 */
var express = require('express');
var challengeController = require('../controller/challengeController');
var indexController = require('../controller/indexController');
var membershipController = require('../controller/membershipSystem');
var leaderboardController = require('../controller/leaderboardController');
var router = express.Router();

// Challenge
router.get('/challenge/:title', challengeController.showChallengePage);
router.post('/challenge/:title', challengeController.checkAnswer);

// Index
router.get('/', indexController.showIndexPage);

// Logout
router.get('/logout', membershipController.logoutUser);

// Leaderboard
router.get('/leaderboard', leaderboardController.showLeaderboardPage);

// Login
router.get('/login', membershipController.showLoginPage);
router.post('/login', membershipController.loginUser);

// Register
router.get('/register', membershipController.showRegisterPage);
router.post('/register', membershipController.registerUser);

module.exports = router;
