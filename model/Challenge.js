/**
 * Created by stone on 25.4.17..
 */

var Promise = require('promise');
var async = require('asyncawait/async');
var await = require('asyncawait/await');

// Helper function to convert stub to title
function set_title(stub) {
    return stub[0].toUpperCase() + stub.slice(1).replace('-', ' ');
}

module.exports = Challenge;

// Class challenge
function Challenge(id, stub, title, problem, answer, points) {
    this.id = id;
    this.stub = stub;
    this.title = title;
    this.problem = problem;
    this.answer = answer;
    this.points = points;
}


/* ---------------------- Sync getChallenges method -------------- */
Challenge.prototype.getChallengesAsync = function (req) {
    return new Promise(function (resolve, reject) {
        req.redis.lrange('challenges', 0, -1, function(err, data) {
            if(err)
                resolve(-1);
            else {
                resolve(data);
            }
        });
    });
};

Challenge.prototype.getChallenges = async (function (req) {
    var data = await (this.getChallengesAsync(req));

    var challengesList = [];
    var count = 1;

    for(var i=0; i<data.length; i++) {
        var challenge = new Challenge();
        challenge.id = count++;
        challenge.stub = data[i];
        challenge.title = set_title(data[i]);
        challengesList.push(challenge);
    }

    return challengesList;
});
/* ---------------------- Sync getChallenges method -------------- */


/* ---------------------- Sync getChallenge method -------------- */
Challenge.prototype.getChallengeAsync = function (req) {
    return new Promise(function (resolve, reject) {
        req.redis.hgetall('challenge:' + req.params.title, function(err, data){
            if(err)
                resolve(-1);
            else {
                resolve(data);
            }
        });
    });
};

Challenge.prototype.getChallenge = async (function (req) {
    var data = await (this.getChallengeAsync(req));

    var challenge = new Challenge();
    challenge.stub = data.stub;
    challenge.title = data.title;
    challenge.problem = data.problem;
    challenge.points = data.points;
    challenge.answer = data.answer;

    return challenge;
});
/* ---------------------- Sync getChallenge method -------------- */


/* ---------------------- Sync alreadyDone method --------------- */
Challenge.prototype.alreadyDoneAsync = function (req, name) {
    var challenge_stub = this.stub;
    return new Promise(function (resolve, reject) {
        req.redis.sismember('done:' + req.session.name, challenge_stub,  function(err, data){
            if(err)
                resolve(-1);
            else {
                resolve(data);
            }
        });
    });
};

Challenge.prototype.alreadyDone = async (function (req) {
    var data = await (this.alreadyDoneAsync(req));
    return data;
});
/* ---------------------- Sync alreadyDone method --------------- */