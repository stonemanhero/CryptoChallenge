/**
 * Created by stone on 25.4.17..
 */
var Promise = require('promise');
var async = require('asyncawait/async');
var await = require('asyncawait/await');

module.exports = User;

// Class User
function User(id, name, password, points) {
    this.id = id;
    this.name = name;
    this.password = password;
    this.points = points;
}

/* ---------------------- Sync exist method ---------------------- */
User.prototype.existsAsync = function (req) {
    var name = this.name;
    return new Promise(function (resolve, reject) {
        req.redis.exists('user:' + name, function(err, data) {
            if(err)
                resolve(-1);
            else {
                resolve(data);
            }
        });
    });
};

User.prototype.exists = async (function (req) {
    var data = await (this.existsAsync(req));
    return data;
});
/* ---------------------- Sync exist method ---------------------- */


/* ---------------------- Register user ---------------------------*/
User.prototype.register = function(req) {
    req.redis.hmset('user:' + this.name, 'name', this.name, 'password', this.password);
    req.redis.zadd('leaderboard', 0, this.name);
};
/* ---------------------- Register user ---------------------------*/


/* ---------------------- Sync login method ---------------------- */
User.prototype.loginAsync = function (req) {
    var name = this.name;
    return new Promise(function (resolve, reject) {
        req.redis.hget('user:' + name, 'password', function(err, data) {
            if(err)
                resolve(-1);
            else {
                resolve(data);
            }
        });
    });
};

User.prototype.login = async (function (req) {
    var data = await (this.loginAsync(req));
    return data;
});
/* ---------------------- Sync login method ---------------------- */


/* ---------------------- Sync getLeaderboard method ------------- */
User.prototype.getLeaderboardAsync = function (req) {
    var name = this.name;
    return new Promise(function (resolve, reject) {
        req.redis.zrevrange('leaderboard', 0, 10, 'withscores', function(err, data) {
            if(err)
                resolve(-1);
            else {
                resolve(data);
            }
        });
    });
};

User.prototype.getLeaderboard = async (function (req) {
    var data = await (this.getLeaderboardAsync(req));

    var leaderboard = [];
    var count = 1;
    var limit;

    if(data.length % 2 === 0)
        limit = data.length;
    else
        limit = data.length + 1;

    for(var i=0; i<limit; i+=2) {
        var user = new User();
        user.id = count++;
        user.name = data[i];
        user.points = data[i+1];
        leaderboard.push(user);
    }

    return leaderboard;
});
/* ---------------------- Sync getLeaderboard method ------------- */


/* ---------------------- Add Points ----------------------------- */
User.prototype.addPoints = function(req, challenge) {
    req.redis.sadd('done:' + this.name, challenge.stub);
    req.redis.zincrby('leaderboard', challenge.points, this.name);
};
/* ---------------------- Add Points ----------------------------- */