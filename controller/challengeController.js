/**
 * Created by stone on 26.4.17..
 */
var User = require('../model/User');
var Challenge = require('../model/Challenge');

// On GET /challenge/:title - only show challenge page
exports.showChallengePage = function(req, res, next) {
    if(req.session.name === undefined) {
        res.redirect('/login');
    }
    else {
        var challenge = new Challenge();
        challenge.stub = req.params.title;
        challenge.getChallenge(req).then(function (data) {
            res.locals = {
                challenge: data,
                name: req.session.name
            };
            res.render('challenge');
        });
    }
};

// On POST /challenge/:title - process answer and show page with status of checking answer
exports.checkAnswer = function(req, res, next) {
    var info = [];
    if(req.session.name === undefined) {
        res.redirect('/login');
    }
    else {
        var challenge = new Challenge();
        challenge.stub = req.params.title;

        if(!req.body.answer) { //Check if answer is in POST
            challenge.getChallenge(req).then(function (data) {
                info[0] = "error";
                info[1] = "Wrong answer";

                res.locals = {
                    challenge: data,
                    name: req.session.name,
                    info: info
                };
                res.render('challenge');
            });
        }
        else { // If answer is in POST check is corret
            challenge.getChallenge(req).then(function (data) {
                challenge.points = data.points;

                if(req.body.answer !== data.answer) {
                    info[0] = "error";
                    info[1] = "Wrong answer";

                    res.locals = {
                        challenge: data,
                        name: req.session.name,
                        info: info
                    };
                    res.render('challenge');
                }
                else { // Check if challenge is already done
                    challenge.alreadyDone(req, req.session.name).then(function(done_status){
                        if(done_status === 1) { // If already done, just show messesage
                            info[0] = "ok";
                            info[1] = "The answer is correct! You already got points for this challenge.";
                        }
                        else { // If is not already done, add it to user's done challenges and increase user's points
                            var user = new User();
                            user.name = req.session.name;
                            user.addPoints(req, challenge);

                            info[0] = "ok";
                            info[1] = "Congratulations! You got " + challenge.points + " points,";
                        }

                        res.locals = {
                            challenge: data,
                            name: req.session.name,
                            info: info
                        };
                        res.render('challenge');
                    });
                }
            });
        }
    }
};