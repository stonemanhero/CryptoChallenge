/**
 * Created by stone on 25.4.17..
 */
var User = require('../model/User');
var hash = require('password-hash');

// On GET /register - just show registration page
exports.showRegisterPage = function(req, res, next) {
    if(req.session.name === undefined) {
        res.render('register');
    }
    else
        res.redirect('/');

};

// On POST /register - try to register user, than show register page with registration status
exports.registerUser  = function(req, res, next) {
    if(req.session.name !== undefined) {
        res.send('User already logged in');
    }
    else {
        var info = [];

        // If post parameters don't exists
        if (!req.body.name || !req.body.password1 || !req.body.password2) {
            info[0] = "error";
            info[1] = "All fields are required";

            res.locals = {
                info: info
            };

            res.render('register');
        }
        else { // If post parameters exists check
            if (req.body.password1 !== req.body.password2) { // If passwords don't match
                info[0] = "error";
                info[1] = "Passwords don't match";

                res.locals = {
                    info: info
                };

                res.render('register');
            }
            else {
                var user = new User();
                user.name = req.body.name.toLowerCase();           // All username should be lowercase
                user.password = hash.generate(req.body.password1); // Hash password for secure storing

                // Check if user is already registered
                user.exists(req).then(function (data) {
                    if (data === 1) { // User is already registered
                        info[0] = "error";
                        info[1] = "There is user with this name";

                        res.locals = {
                            info: info
                        };

                        res.render('register');
                    }
                    else { // We can now register new user
                        user.register(req); // There is no need for syncing because we are only inserting data to redis

                        info[0] = "ok";
                        info[1] = "Your registration has been successful!";

                        res.locals = {
                            info: info
                        };

                        res.render('register');
                    }
                });
            }
        }
    }
};

// On GET /login - just show login page
exports.showLoginPage = function(req, res, next) {
    if(req.session.name !== undefined) {
        res.redirect('/');
    }
    else {
        res.render('login');
    }
};

// On POST /login - try to login user, if success redirect to index, else show login page with login status
exports.loginUser = function(req, res, next) {
    if(req.session.name !== undefined) {
        res.send('User already logged in');
    }
    else {
        var info = [];

        // If post parameters don't exists
        if (!req.body.name || !req.body.password) {
            info[0] = "error";
            info[1] = "Authentication failed";

            res.locals = {
                info: info
            };

            res.render('login');
        }
        else {
            var user = new User();
            user.name = req.body.name;
            user.password = req.body.password;

            user.login(req).then(function (data) {
                if (data !== null) {
                    if (hash.verify(user.password, data)) {
                        info[0] = "ok";
                        info[1] = "You are now logged in";

                        res.locals = {
                            info: info
                        };

                        // Set session for auth with user name
                        req.session.name = user.name;
                        console.log(req.session.name);
                        res.redirect('/');
                    }
                    else {
                        info[0] = "error";
                        info[1] = "Authentication failed";

                        res.locals = {
                            info: info
                        };

                        res.render('login');
                    }
                }
                else {
                    info[0] = "error";
                    info[1] = "Authentication failed";

                    res.locals = {
                        info: info
                    };

                    res.render('login');
                }
            });
        }
    }
};

// On GET /logout - logout user and redirect to login page
exports.logoutUser = function(req, res, next) {
    if(req.session.name === undefined) {
        res.render('login');
    }
    else {
        var info = [];
        req.session.destroy();
        info[0] = "ok";
        info[1] = "You are now logged out";

        res.locals = {
            info: info
        };

        res.render('login');
    }
};
