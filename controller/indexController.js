/**
 * Created by stone on 26.4.17..
 */
var Challenge = require('../model/Challenge');

// On GET / - show index page with all challenges
exports.showIndexPage = function(req, res, next) {
    if(req.session.name === undefined) {
        res.redirect('/login');
    }
    else {
        var challenge = new Challenge();
        challenge.getChallenges(req).then(function (data) {
            res.locals = {
                challenges: data,
                name: req.session.name
            };
            res.render('index');
        });
    }
};