/**
 * Created by stone on 26.4.17..
 */
var User = require('../model/User');

// On GET /leaderboar - show leaderboard page with top 10 users
exports.showLeaderboardPage = function(req, res, next) {
    if(req.session.name === undefined) {
        res.redirect('/login');
    }
    else {
        var user = new User();
        user.name = req.session.name;
        user.getLeaderboard(req).then(function (data) {
            res.locals = {
                leaderboard: data,
                name: req.session.name
            };
            res.render('leaderboard');
        });
    }
};